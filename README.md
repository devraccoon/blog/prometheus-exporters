# Prometheus Exporters

![Dashboard example](./docs/img/grafana.png)

## Introduction

This project contains different Prometheus Exporters. They are used to expose some sample metrics automatically, without effort.

### Exporters

- [Chi Exporter](#chi-exporter): A Middleware designed for *Chi* Router. It creates and exposes metrics abouts requests received, it's urls, methods, and sender's IP.

- [Echo Exporter](#Echo-exporter): A Middleware designed for *Echo* Router. It creates and exposes metrics abouts requests received, it's urls, methods, and sender's IP.

- [Mgo Exporter](#mgo-exporter): Mongo exporter. It watches all collections in a choosen database, and exports the number of documents in each one.

#### Chi Exporter

The *Chi Exporter* is a middleware which is introduced in your *Chi router*.

It has the following exported metrics:

- `total_requests`: Counter of all requests processed by the router. It's labeled by `url`, `method`, `status_code`.
- `requests_duration`: Histogram containing observations of the duration of all requests processed by the router.
- `requests_payload`: Histogram containing observations of the payload's size of all requests processed by the router.

#### Echo Exporter

The *Echo Exporter* is a middleware which is introduced in your *Echo router*.

It has the following exported metrics:

- `total_requests`: Counter of all requests processed by the router. It's labeled by `url`, `method`, `status_code`.
- `requests_duration`: Histogram containing observations of the duration of all requests processed by the router.
- `requests_payload`: Histogram containing observations of the payload's size of all requests processed by the router.

#### Mgo Exporter

The *Mgo Exporter* is an exported which receives an *Mgo session* and watches all collections of one provided database.

It has the following exported metrics:

- `collection_count`: Gauge containing the total number of documents in each collection for the provided database. It's labeled by `collection`.
