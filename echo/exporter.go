package exporters

import (
	"strconv"
	"time"

	"github.com/labstack/echo"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestsCounterVec = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "total_requests",
			Help: "Total number of requests processed by the app",
		},
		[]string{"url", "method", "status_code"},
	)
	requestsDurationHistogram = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "requests_duration",
			Help:    "Histogram of app's requests duration, in milliseconds",
			Buckets: prometheus.ExponentialBuckets(10, 1.8, 15),
		})

	requestsPayloadHistogram = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "requests_payload",
			Help:    "Histogram of app's requests payload size, in bytes",
			Buckets: prometheus.ExponentialBuckets(1000, 2, 15),
		})
)

func Middleware() echo.MiddlewareFunc {
	prometheus.MustRegister(requestsCounterVec)
	prometheus.MustRegister(requestsDurationHistogram)
	prometheus.MustRegister(requestsPayloadHistogram)
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			t1 := time.Now()
			defer func() {
				duration := float64(time.Since(t1) / time.Millisecond)
				req := c.Request()
				res := c.Response()
				path := c.Path()
				statusCode := strconv.Itoa(res.Status)
				method := req.Method
				size := float64(res.Size)
				requestsCounterVec.WithLabelValues(path, method, statusCode).Inc()
				requestsDurationHistogram.Observe(duration)
				requestsPayloadHistogram.Observe(size)
			}()
			return next(c)
		}
	}
}
