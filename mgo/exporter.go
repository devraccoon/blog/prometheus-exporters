package exporters

import (
	"github.com/globalsign/mgo"
	"github.com/prometheus/client_golang/prometheus"
	"log"
	"time"
)

type MgoExporter struct {
	Session  *mgo.Session
	Db       string
	Interval time.Duration
}

var (
	CollectionCountGaugeVec = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "collection_count",
			Help: "Total number of documents in collection",
		},
		[]string{"collection"},
	)
)

func (m MgoExporter) Start() {
	colls, err := m.Session.DB(m.Db).CollectionNames()
	if err != nil {
		panic(err)
	}
	go func() {
		for {
			for _, c := range colls {
				count, err := m.Session.DB(m.Db).C(c).Count()
				if err != nil {
					log.Println(err)
				}
				CollectionCountGaugeVec.WithLabelValues(c).Set(float64(count))
			}
			time.Sleep(m.Interval)
		}
	}()
	prometheus.MustRegister(CollectionCountGaugeVec)
}
