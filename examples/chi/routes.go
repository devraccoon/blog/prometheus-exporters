package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
	"time"
)

func Pong(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("\n-- PONG --\n")
	w.WriteHeader(http.StatusTeapot)
}

func Ping(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("\n-- PING --\n")
	w.WriteHeader(http.StatusOK)
}

func Wait(w http.ResponseWriter, r *http.Request) {
	t, err := strconv.Atoi(chi.URLParam(r, "time"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Printf("\n-- Waiting %v ms --\n", t)
	time.Sleep(time.Duration(t) * time.Millisecond)
}
