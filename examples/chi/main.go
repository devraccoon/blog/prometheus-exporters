package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	promChi "gitlab.com/devraccoon/blog/prometheus-exporters/chi"
	"net/http"
)

func main() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	// These are the two lines that really matter
	r.Use(promChi.Middleware)                // All requests must go through the exporter middleware
	r.Handle("/metrics", promhttp.Handler()) // Expose the /metrics route, and let the promhttp take care of it

	r.Get("/pong", Pong)
	r.Get("/ping", Ping)
	r.Get("/wait/{time}", Wait)
	http.ListenAndServe(":9001", r) // Serve your application
}
