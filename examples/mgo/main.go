package main

import (
	"github.com/globalsign/mgo"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	promMgo "gitlab.com/devraccoon/blog/prometheus-exporters/mgo"
	"net/http"
	"time"
)

var (
	DBHost = "localhost:27017"
)

func main() {
	session, err := mgo.Dial(DBHost)
	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	// Collection People
	c := session.DB("test").C("people")
	mgoExporter := promMgo.MgoExporter{
		Session:  session,
		Db:       "test",
		Interval: 1000 * time.Millisecond,
	}
	mgoExporter.Start()
	server := http.NewServeMux()
	server.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":9001", server)

	// Insert Datas
	for {
		err = c.Insert(&Person{Name: "Ale", Phone: "+55 53 1234 4321", Timestamp: time.Now()},
			&Person{Name: "Cla", Phone: "+66 33 1234 5678", Timestamp: time.Now()})
		if err != nil {
			panic(err)
		}
		time.Sleep(2500 * time.Millisecond)
	}
}
