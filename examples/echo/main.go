package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	promEcho "gitlab.com/devraccoon/blog/prometheus-exporters/echo"
	"net/http"
)

func main() {
	r := echo.New()
	r.Use(middleware.Logger())

	// These are the two lines that really matter
	r.Use(promEcho.Middleware())                            // All requests must go through the exporter middleware
	r.GET("/metrics", echo.WrapHandler(promhttp.Handler())) // Expose the /metrics route, and let the promhttp take care of it

	r.GET("/ping", Ping)
	r.GET("/pong", Pong)
	r.GET("/wait/:time", Wait)
	http.ListenAndServe(":9001", r) // Serve your application
}
