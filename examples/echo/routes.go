package main

import (
	"fmt"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
	"time"
)

func Pong(c echo.Context) error {
	fmt.Printf("\n-- PONG --\n")
	return c.String(http.StatusTeapot, "Pong")
}

func Ping(c echo.Context) error {
	fmt.Printf("\n-- PING --\n")
	return c.String(http.StatusOK, "Ping")
}

func Wait(c echo.Context) error {
	t, err := strconv.Atoi(c.Param("time"))
	if err != nil {
		return c.String(http.StatusBadRequest, "Invalid time parameter")
	}
	defer func() {
		fmt.Printf("\n-- Waiting %v ms --\n", t)
		time.Sleep(time.Duration(t) * time.Millisecond)
	}()
	return c.String(http.StatusOK, "")
}
