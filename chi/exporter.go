package exporters

import (
	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
	"strconv"
	"time"
)

var (
	requestsCounterVec = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "total_requests",
			Help: "Total number of requests processed by the app",
		},
		[]string{"url", "method", "status_code"},
	)
	requestsDurationHistogram = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "requests_duration",
			Help:    "Histogram of app's requests duration",
			Buckets: prometheus.LinearBuckets(10, 100, 15),
		})

	requestsPayloadHistogram = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "requests_payload",
			Help:    "Histogram of app's requests payload size, in bytes",
			Buckets: prometheus.LinearBuckets(1000, 10000, 15),
		})
)

func Middleware(next http.Handler) http.Handler {
	prometheus.MustRegister(requestsCounterVec)
	prometheus.MustRegister(requestsDurationHistogram)
	prometheus.MustRegister(requestsPayloadHistogram)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
		t1 := time.Now()
		defer func() {
			duration := float64(time.Since(t1) / time.Millisecond)
			url := r.URL.Path
			method := r.Method
			statusCode := strconv.Itoa(ww.Status())
			size := float64(ww.BytesWritten())
			requestsCounterVec.WithLabelValues(url, method, statusCode).Inc()
			requestsDurationHistogram.Observe(duration)
			requestsPayloadHistogram.Observe(size)
		}()
		next.ServeHTTP(ww, r)
	})
}
